# notepad.exe
Open source Notepad clone for GNU/Linux

![screenshot](screenshot.png)

# Features
* written vala gtk3
* sourceview4 based text editor
* line numbers and syntax highlighting
* dark / light themes
* \> 1mb size

## How to build
```shell
make
make install
```
