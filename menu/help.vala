public Gtk.MenuItem help_menu() {
    var item_help = new Gtk.MenuItem.with_label ("Help");

    var help_menu = new Gtk.Menu ();
    item_help.set_submenu (help_menu);

    
    var item_about = new Gtk.MenuItem.with_label ("About");
    help_menu.add(item_about);

    
    item_about.activate.connect (() => {
        Gtk.AboutDialog dialog = new Gtk.AboutDialog ();
	dialog.set_destroy_with_parent (true);
	dialog.set_transient_for (notepad);
	dialog.set_modal (true);
	
	dialog.program_name = "notepad.exe";
	dialog.set_logo_icon_name("notepad.exe");
	dialog.comments = "Notepad.exe clone for linux";
	//dialog.version = "";

	dialog.website = "https://gitlab.com/sulincix/notepad";
	dialog.website_label = "sulincix - notepad";

	dialog.response.connect ((response_id) => {
		if (response_id == Gtk.ResponseType.CANCEL || response_id == Gtk.ResponseType.DELETE_EVENT) {
			dialog.hide_on_delete ();
		}
	});

	// Show the dialog:
	dialog.present ();       
    });
    return item_help;

}
