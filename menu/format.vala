public Gtk.MenuItem format_menu() {
    var item_format = new Gtk.MenuItem.with_label ("Format");

    var format_menu = new Gtk.Menu ();
    item_format.set_submenu (format_menu);

    var item_wrap = new Gtk.CheckMenuItem.with_label("Word Wrap");
    format_menu.add(item_wrap);
    item_wrap.set_active(settings.get_boolean("word-wrap"));
    
    var item_font = new Gtk.MenuItem.with_label ("Font...");
    format_menu.add(item_font);

    item_wrap.activate.connect (() => {
        settings.set_boolean("word-wrap",item_wrap.get_active());
        if(item_wrap.get_active()){
            notepad.text_view.set_wrap_mode(Gtk.WrapMode.WORD);
        }else{
            notepad.text_view.set_wrap_mode(Gtk.WrapMode.NONE);
        }
    });

    item_font.activate.connect (() => {
        var dialog = new Gtk.FontChooserDialog("Select Font", notepad);
        dialog.response.connect((response)=>{
            if(response != CANCEL){
                stdout.printf("%d %s\n",response,dialog.get_font());
                settings.set_string("font",dialog.get_font());
                notepad.set_font(dialog.get_font());
            }
            dialog.destroy();
        });
        dialog.run();
    });
    return item_format;

}
