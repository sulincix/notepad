public Gtk.MenuItem file_menu() {
    var item_file = new Gtk.MenuItem.with_label ("File");
    

    var file_menu = new Gtk.Menu ();
    item_file.set_submenu (file_menu);

    var item_new = new Gtk.MenuItem.with_label ("New");
    file_menu.add (item_new);

    var item_open = new Gtk.MenuItem.with_label ("Open...");
    file_menu.add (item_open);

    var item_save = new Gtk.MenuItem.with_label ("Save");
    file_menu.add (item_save);

    var item_save_as = new Gtk.MenuItem.with_label ("Save as");
    file_menu.add (item_save_as);

    var item_exit = new Gtk.MenuItem.with_label ("Exit");
    file_menu.add (item_exit);

    item_new.activate.connect (() => {
        save_dialog_event();
    });
    
    item_open.activate.connect (() => {
        save_dialog_event();
        open_file();
    });
    item_save_as.activate.connect (() => {
        save_as();
    });
    item_save.activate.connect(()=> {
        save();
    });
    item_exit.activate.connect (()=>{
        notepad.quit();
    });

    item_file.add_accelerator("activate", keyGroup, Gdk.Key.Alt_L | Gdk.Key.Alt_R, 0, Gtk.AccelFlags.VISIBLE);
    item_new.add_accelerator("activate", keyGroup, 'N', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
    item_open.add_accelerator("activate", keyGroup, 'O', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
    item_save.add_accelerator("activate", keyGroup, 'S', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
    item_save_as.add_accelerator("activate", keyGroup, 'S', Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK, Gtk.AccelFlags.VISIBLE);
    item_exit.add_accelerator("activate", keyGroup, 'Q', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);

    return item_file;
}
