public Gtk.MenuItem view_menu() {
    string theme;
    var item_view = new Gtk.MenuItem.with_label ("View");

    var view_menu = new Gtk.Menu ();
    item_view.set_submenu (view_menu);
    
    var item_theme = new Gtk.MenuItem();
    view_menu.add (item_theme);
    theme = settings.get_string("theme");
    if(theme == "dark"){
        item_theme.set_label("Light Theme");
    }else{
        item_theme.set_label("Dark Theme");
    }

    var item_highlight = new Gtk.CheckMenuItem.with_label("Syntax highlighting");
    item_highlight.set_active(settings.get_boolean("highlight"));
    view_menu.add (item_highlight);

    var item_linenum = new Gtk.CheckMenuItem.with_label("Line numbers");
    item_linenum.set_active(settings.get_boolean("line-number"));
    view_menu.add (item_linenum);

    item_theme.activate.connect (() => {
        theme = settings.get_string("theme");
        if(theme == "dark"){
            settings.set_string("theme","light");
            item_theme.set_label("Dark Theme");
        }else{
            settings.set_string("theme","dark");
            item_theme.set_label("Light Theme");
        }
        style_init();
    });

    item_highlight.activate.connect (() => {
        settings.set_boolean("highlight",item_highlight.get_active());
        notepad.set_highlight();
    });

    item_linenum.activate.connect (() => {
        settings.set_boolean("line-number",item_linenum.get_active());
        notepad.set_linenum();
    });

    item_theme.add_accelerator("activate", keyGroup, 'T', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);

    return item_view;

}
