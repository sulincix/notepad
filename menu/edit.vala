public Gtk.MenuItem edit_menu() {
    var item_edit = new Gtk.MenuItem.with_label("Edit");

    var edit_menu = new Gtk.Menu ();
    item_edit.set_submenu (edit_menu);

    var item_undo = new Gtk.MenuItem.with_label("Undo");
    edit_menu.add (item_undo);

    var item_redo = new Gtk.MenuItem.with_label("Redo");
    edit_menu.add (item_redo);
    
    var item_cut = new Gtk.MenuItem.with_label("Cut");
    edit_menu.add (item_cut);
    
    var item_copy = new Gtk.MenuItem.with_label("Copy");
    edit_menu.add (item_copy);
    
    var item_paste = new Gtk.MenuItem.with_label("Paste");
    edit_menu.add (item_paste);
    
    var item_find = new Gtk.MenuItem.with_label("Find");
    edit_menu.add (item_find);
    
    var item_replace = new Gtk.MenuItem.with_label("Replace");
    edit_menu.add (item_replace);
    
    var item_find_next = new Gtk.MenuItem.with_label("Find Next");
    edit_menu.add (item_find_next);
    
    var item_select_all = new Gtk.MenuItem.with_label("Select All");
    edit_menu.add (item_select_all);
    
    var item_date_time = new Gtk.MenuItem.with_label("Date / Time");
    edit_menu.add (item_date_time);

    var item_search = new Gtk.MenuItem.with_label("Search on Web");
    edit_menu.add (item_search);

    item_undo.activate.connect (() => {
        notepad.text_view.undo();
    });
    item_redo.activate.connect (() => {
        notepad.text_view.redo();
    });

    item_cut.activate.connect (() => {
        notepad.text_view.cut_clipboard();
    });
    item_copy.activate.connect (() => {
        notepad.text_view.copy_clipboard();
    });
    item_paste.activate.connect (() => {
        notepad.text_view.paste_clipboard();
    });

    item_find.activate.connect (() => {
        find_window.show();
    });
    item_replace.activate.connect (() => {
        replace_window.show();
    });
    item_find_next.activate.connect (() => {
        Gtk.TextSearchFlags flag = Gtk.TextSearchFlags.TEXT_ONLY;
            if(find_window.match_case.get_active()){
                flag = Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE;
            }
            if(find_window.way_up.get_active()){
                find_window.find_next(notepad.get_selected_text(),flag);
            }if(find_window.way_down.get_active()){
                find_window.find_previous(notepad.get_selected_text(),flag);
            }
    });
    
    item_select_all.activate.connect (() => {
        Gtk.TextIter start_iter, end_iter;
        notepad.text_view.buffer.get_start_iter(out start_iter);
        notepad.text_view.buffer.get_end_iter(out end_iter);
        notepad.text_view.buffer.select_range(start_iter, end_iter);
    });
    
    item_date_time.activate.connect (() => {
        var now = new DateTime.now_local ();
        string data = now.format("%H:%M %Y.%m.%d");
        notepad.text_view.buffer.insert_at_cursor(data, data.length);
    });
    
    item_search.activate.connect (() => {
        string[] args = {
        "xdg-open", "https://duckduckgo.com/?q="+url_encode(notepad.get_selected_text())
        };
        string[] env = Environ.get ();
        try{
            GLib.Process.spawn_async(
                "/",
                args,
                env,
                SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
                null,null);
        }catch{
            stdout.printf("Failed to run xdg-open command!\n");
        }
    });
    
    item_undo.add_accelerator("activate", keyGroup, 'Z', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
    item_redo.add_accelerator("activate", keyGroup, 'Y', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_redo.add_accelerator("activate", keyGroup, 'Z', Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK , Gtk.AccelFlags.VISIBLE);
    item_cut.add_accelerator("activate", keyGroup, 'X', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_copy.add_accelerator("activate", keyGroup, 'C', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_paste.add_accelerator("activate", keyGroup, 'V', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_find.add_accelerator("activate", keyGroup, 'F', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_find_next.add_accelerator("activate", keyGroup, Gdk.Key.F3, 0 , Gtk.AccelFlags.VISIBLE);
    item_replace.add_accelerator("activate", keyGroup, 'H', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_replace.add_accelerator("activate", keyGroup, 'R', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_select_all.add_accelerator("activate", keyGroup, 'A', Gdk.ModifierType.CONTROL_MASK , Gtk.AccelFlags.VISIBLE);
    item_date_time.add_accelerator("activate", keyGroup, Gdk.Key.F5, 0 , Gtk.AccelFlags.VISIBLE);
    item_search.add_accelerator("activate", keyGroup, 'F', Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK, Gtk.AccelFlags.VISIBLE);

    return item_edit;
}
