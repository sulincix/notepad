public extern string get_theme_data();
public void style_init(){
    string theme = settings.get_string("theme");
    if(theme == "system"){
        return;
    }
    try{
        theme_init();
        Gtk.CssProvider css_provider = new Gtk.CssProvider ();
        if(theme == "light"){
            css_provider.load_from_data(theme_light_data);
        }else if (theme == "dark"){
            css_provider.load_from_data(theme_dark_data);
        }
        Gtk.StyleContext.add_provider_for_screen (
            Gdk.Screen.get_default (),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );
    }catch{
    
    }
}
