public void open_file(){
    Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog (
        "Open", null, Gtk.FileChooserAction.OPEN,
        "_Cancel",
        Gtk.ResponseType.CANCEL,
        "_Open",
        Gtk.ResponseType.ACCEPT);
    chooser.set_select_multiple (false);
    chooser.run ();
    chooser.close ();
    if (chooser.get_file () != null) {
        notepad.file = chooser.get_file ();
        try {
            uint8[] contents;
            string etag_out;
            notepad.file.load_contents (null, out contents, out etag_out);
            notepad.text_view.buffer.text = (string) contents;
            notepad.label.set_text(notepad.get_name()+" - Notepad");
        } catch (Error e) {
            dialog_error (e.message);
        }
    }
    notepad.set_highlight();
}

public void open_path(string path){
    notepad.file = File.new_for_path(path);
    if (!notepad.file.query_exists ()) {
        return;
    }
    try {
        uint8[] contents;
        string etag_out;
        notepad.file.load_contents (null, out contents, out etag_out);
        notepad.text_view.buffer.text = (string) contents;
        notepad.label.set_text(notepad.get_name()+" - Notepad");
    } catch (Error e) {
        dialog_error (e.message);
    }
    notepad.set_highlight();
}

public bool save_as(){
    Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog (
        "Save As", null, Gtk.FileChooserAction.SAVE,
        "_Cancel",
        Gtk.ResponseType.CANCEL,
        "_Save",
        Gtk.ResponseType.ACCEPT);
    chooser.set_select_multiple (false);
    chooser.run ();
    chooser.close ();
    if (chooser.get_file () != null) {
        notepad.file = chooser.get_file ();
        try {
            var file_stream = notepad.file.create (FileCreateFlags.NONE);
            var data_stream = new DataOutputStream (file_stream);
        data_stream.put_string (notepad.text_view.buffer.text);
        } catch (Error e) {
            dialog_error (e.message);
            return false;
        }
    }
    return true;
}
public bool save(){
    if (notepad.file != null) {
        try {
            notepad.file.replace_contents (notepad.text_view.buffer.text.data, null, false, FileCreateFlags.NONE, null);
            return true;
        } catch (Error e) {
            dialog_error (e.message);
            return false;
        }
    }else{
        return save_as();
    }
}
public bool save_dialog_event(){
    int res = dialog_yesno("Save", "Don't Save","The text in the %s file has been changed.\nDo you want to save the changes?".printf(notepad.get_name()));
    if (res == YES){
        return save();
    }else if(res == NO){
        notepad.text_view.buffer.text = "";
        return true;
    }else{
        return false;
    }
}
