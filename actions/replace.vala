public class ReplaceWindow : Gtk.Window {
    private Gtk.TextIter iter;
    public Gtk.Entry search_entry;
    public Gtk.Entry value_entry;
    public Gtk.CheckButton match_case;

    public void init(){
        // window init
        set_default_size (-1, -1);
        window_position = Gtk.WindowPosition.CENTER;
        set_title(" Replace ");
        set_resizable(false);
        var header = new Gtk.HeaderBar();
        header.pack_start(new Gtk.Label("  "));
        header.pack_start(new Gtk.Image.from_icon_name("notepad.exe",Gtk.IconSize.MENU));
        var label = new Gtk.Label("Replace ");
        header.pack_start(label);
        header.set_show_close_button(false);
        set_titlebar(header);

        // Main Horitontal box
        var main_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        // Button container box (Vertical)
        var button_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        // Row box (Vertical)
        var row_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        main_box.pack_start(row_box,false,false,5);
        main_box.pack_start(button_box,false,false,5);
        
        // entry & label box
        var entry_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var label_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var entry_label_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        
        entry_label_box.pack_start(label_box,false,false,0);
        entry_label_box.pack_start(entry_box,false,false,0);
        row_box.pack_start(entry_label_box,false,false,0);        
        
        // exit button
        var exit_button = new Gtk.Button();
        exit_button.set_can_focus(false);
        exit_button.clicked.connect(()=>{
            hide();
        });
        exit_button.add(new Gtk.Image.from_icon_name("window-close-symbolic",Gtk.IconSize.MENU));
        exit_button.set_relief(Gtk.ReliefStyle.NONE);
        header.pack_end(exit_button);
        
        // first row
        search_entry = new Gtk.Entry();
        var lf = new Gtk.Label("Find:");
        lf.set_justify(Gtk.Justification.LEFT);
        lf.set_xalign(0);
        label_box.pack_start(lf,true,true,0);
        entry_box.pack_start(search_entry,true,true,0);
        
        // second row
        value_entry = new Gtk.Entry();
        var lr = new Gtk.Label("New value:");
        lr.set_justify(Gtk.Justification.LEFT);
        lf.set_xalign(0);
        label_box.pack_start(lr,true,true,0);
        entry_box.pack_start(value_entry,true,true,0);
        
        // match case
        match_case = new Gtk.CheckButton();
        match_case.set_label("Match Case");
        row_box.pack_start(match_case,false,false,0);
        
        // buttons
        var search_button = new Gtk.Button();
        search_button.set_label("Find Next");
        button_box.add(search_button);
        
        var replace_button = new Gtk.Button();
        replace_button.set_label("Replace");
        button_box.add(replace_button);

        var replace_all_button = new Gtk.Button();
        replace_all_button.set_label("Replace All");
        button_box.add(replace_all_button);
        
        var cancel_button = new Gtk.Button();
        cancel_button.set_label("Cancel");
        button_box.add(cancel_button);
        cancel_button.clicked.connect(()=>{
            hide();
        });
        
        
        // event definitions
        search_button.clicked.connect(()=>{
            Gtk.TextSearchFlags flag = Gtk.TextSearchFlags.TEXT_ONLY;
            if(match_case.get_active()){
                flag = Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE;
            }
            find_window.find_next(search_entry.get_text(),flag);
        });
        replace_button.clicked.connect(()=>{
            Gtk.TextSearchFlags flag = Gtk.TextSearchFlags.TEXT_ONLY;
            if(match_case.get_active()){
                flag = Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE;
            }
            replace_next(search_entry.get_text(),value_entry.get_text(),flag);
        });
        replace_all_button.clicked.connect(()=>{
            Gtk.TextSearchFlags flag = Gtk.TextSearchFlags.TEXT_ONLY;
            if(match_case.get_active()){
                flag = Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE;
            }
            replace_all(search_entry.get_text(),value_entry.get_text(),flag);
        });
        delete_event.connect(()=>{
            hide();
            return true;
        });
        
        // finish
        add(main_box);
        show_all();
        hide();
    }
    public void replace_next(string text, string value,Gtk.TextSearchFlags flag){
        Gtk.TextIter end_iter;
        int offset = notepad.text_view.buffer.cursor_position;
        notepad.text_view.buffer.get_iter_at_offset (out iter, offset);
        if(iter.forward_search(text,flag,out iter,out end_iter,null)){
            notepad.text_view.buffer.delete(ref iter, ref end_iter);
            notepad.text_view.buffer.insert_at_cursor(value, value.length);
        }
        find_window.find_next(search_entry.get_text(),flag);
    }
    public void replace_all(string text, string value,Gtk.TextSearchFlags flag){
        while(find_window.can_find(text,flag)){
            replace_next(text, value,flag);
        }
    }
}
