public class FindWindow : Gtk.Window {
    private Gtk.TextIter iter;
    public Gtk.Entry search_entry;
    public Gtk.CheckButton match_case;
    public Gtk.RadioButton way_up;
    public Gtk.RadioButton way_down;

    public void init(){
        // window init
        set_default_size (-1, -1);
        window_position = Gtk.WindowPosition.CENTER;
        set_title(" Find ");
        set_resizable(false);
        var header = new Gtk.HeaderBar();
        header.pack_start(new Gtk.Label("  "));
        header.pack_start(new Gtk.Image.from_icon_name("notepad.exe",Gtk.IconSize.MENU));
        header.pack_start(new Gtk.Label("Find "));
        header.set_show_close_button(false);
        set_titlebar(header);
        
        // exit button
        var exit_button = new Gtk.Button();
        exit_button.set_can_focus(false);
        exit_button.clicked.connect(()=>{
            hide();
        });
        exit_button.add(new Gtk.Image.from_icon_name("window-close-symbolic",Gtk.IconSize.MENU));
        exit_button.set_relief(Gtk.ReliefStyle.NONE);
        header.pack_end(exit_button);
    
        // First row:
        search_entry = new Gtk.Entry();

        var search_button = new Gtk.Button();
        search_button.set_label("Find Next");
    
        var main_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var search_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        var option_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);

        search_box.pack_start(new Gtk.Label("Find:"),false,false,5);
        search_box.pack_start(search_entry,true,true,5);
        search_box.pack_end(search_button,false,false,5);

        // Second row:
        match_case = new Gtk.CheckButton();
        match_case.set_label("Match Case");
        
        way_up = new Gtk.RadioButton(null);
        way_down = new Gtk.RadioButton(way_up.get_group());
        way_up.set_label("Up");
        way_down.set_label("Down");

        var cancel_button = new Gtk.Button();
        cancel_button.set_label("Cancel");

        option_box.pack_start(match_case,false,false,5);
        option_box.pack_start(way_up,false,false,5);
        option_box.pack_start(way_down,false,false,5);
        option_box.pack_end(cancel_button,false,false,5);

        main_box.pack_start(search_box,false,false,5);
        main_box.pack_start(option_box,true,true,5);
        
        add(main_box);
        
        cancel_button.clicked.connect(()=>{
            hide();
        });
        
        delete_event.connect(()=>{
            hide();
            return true;
        });
        search_button.clicked.connect(()=>{
            Gtk.TextSearchFlags flag = Gtk.TextSearchFlags.TEXT_ONLY;
            if(match_case.get_active()){
                flag = Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE;
            }
            if(way_up.get_active()){
                find_next(search_entry.get_text(),flag);
            }if(way_down.get_active()){
                find_previous(search_entry.get_text(),flag);
            }
        });
        show_all();
        hide();
        
    }
    public bool can_find(string text,Gtk.TextSearchFlags flag){
        var text_buffer = new Gtk.TextBuffer (null);
        text_buffer.set_text (text);
        notepad.text_view.buffer.get_start_iter(out iter);
        return iter.forward_search(text,flag,null,null,null);
    }

    public void find_next(string text,Gtk.TextSearchFlags flag){
        Gtk.TextIter end_iter;
        int offset = notepad.text_view.buffer.cursor_position;
        offset += notepad.length(notepad.get_selected_text());
        notepad.text_view.buffer.get_iter_at_offset (out iter, offset);
        if(iter.forward_search(text,flag,out iter,out end_iter,null)){
            notepad.text_view.buffer.select_range(iter, end_iter);
        }else{
            if(can_find(text,flag)){
                notepad.text_view.buffer.get_start_iter(out iter);
                notepad.text_view.buffer.place_cursor(iter);
                find_next(text,flag);
            }
        }
    }
    public void find_previous(string text, Gtk.TextSearchFlags flag){
        Gtk.TextIter start_iter, end_iter;
        int offset = notepad.text_view.buffer.cursor_position;
        if(offset == 0){
            offset = notepad.length(notepad.text_view.buffer.text);
        }
        notepad.text_view.buffer.get_iter_at_offset (out iter, offset);
        if(iter.backward_search(text,flag,out start_iter,out end_iter,null)){
            notepad.text_view.buffer.select_range(start_iter, end_iter);
        }else{
            if(can_find(text,flag)){
                notepad.text_view.buffer.get_end_iter(out iter);
                notepad.text_view.buffer.place_cursor(iter);
                find_next(text,flag);
            }
        }
    }
}
