public int YES = Gtk.ResponseType.YES;
public int NO = Gtk.ResponseType.NO;
public int CANCEL = Gtk.ResponseType.CANCEL;
public int dialog_yesno(string yes, string no, string label){
    var dialog = new Gtk.Dialog();
    var hb = new Gtk.HeaderBar();

    // headerbar
    hb.set_show_close_button(false);
    dialog.set_titlebar(hb);
    hb.pack_start(new Gtk.Label("  "));
    hb.pack_start(new Gtk.Image.from_icon_name("notepad.exe",Gtk.IconSize.MENU));
        
    var dl = new Gtk.Label("Notepad");
    hb.pack_start(dl);

    // Window features
    dialog.set_default_size(400,100);
    dialog.set_resizable(false);
    dialog.set_keep_above(true);

    // content area
    var l = new Gtk.Label("");
    dialog.get_content_area().pack_start(l,false,false,5);
    l.set_text(label);

    // buttons
    dialog.add_button(yes,YES);
    dialog.add_button(no,NO);
    dialog.add_button("Cancel",CANCEL);
    
        // exit button
    var exit_button = new Gtk.Button();
    exit_button.set_can_focus(false);
    exit_button.clicked.connect(()=>{
        dialog.destroy();
    });
    exit_button.add(new Gtk.Image.from_icon_name("window-close-symbolic",Gtk.IconSize.MENU));
    exit_button.set_relief(Gtk.ReliefStyle.NONE);
    hb.pack_end(exit_button);
    
    // show and return response
    dialog.show_all();
    int result = dialog.run();
    dialog.destroy();
    return result;
}

public int dialog_error(string label){
    var dialog = new Gtk.Dialog();
    var hb = new Gtk.HeaderBar();

    // headerbar
    hb.set_show_close_button(false);
    dialog.set_titlebar(hb);
    hb.pack_start(new Gtk.Label("  "));
    hb.pack_start(new Gtk.Image.from_icon_name("notepad.exe",Gtk.IconSize.MENU));
    
    // exit button
    var exit_button = new Gtk.Button();
    exit_button.set_can_focus(false);
    exit_button.clicked.connect(()=>{
        dialog.destroy();
    });
    exit_button.add(new Gtk.Image.from_icon_name("window-close-symbolic",Gtk.IconSize.MENU));
    exit_button.set_relief(Gtk.ReliefStyle.NONE);
    hb.pack_end(exit_button);
        
    var dl = new Gtk.Label("Notepad");
    hb.pack_start(dl);

    // Window features
    dialog.set_default_size(400,100);
    dialog.set_resizable(false);
    dialog.set_keep_above(true);

    // content area
    var l = new Gtk.Label("");
    dialog.get_content_area().pack_start(l,false,false,5);
    l.set_text(label);

    // buttons
    dialog.add_button("OK",CANCEL);
    
    // show and return response
    dialog.show_all();
    int result = dialog.run();
    dialog.destroy();
    return result;
}
