public class NotePad : Gtk.Window {

    public Gtk.SourceView text_view;
    public Gtk.MenuBar menu;
    public Gtk.HeaderBar header;
    public Pango.FontDescription font;
    public Gtk.Label label;
    public File file;

    public NotePad () {}


    public void init() {
        set_default_size (settings.get_int("height"), settings.get_int("width"));
        window_position = Gtk.WindowPosition.CENTER;  
        set_title(get_name()+" - Notepad");
        header = new Gtk.HeaderBar();
        label = new Gtk.Label(get_name()+" - Notepad");
        header.pack_start(new Gtk.Label("  "));
        header.pack_start(new Gtk.Image.from_icon_name("notepad.exe",Gtk.IconSize.MENU));
        header.pack_start(label);

        // exit button
        header.set_show_close_button(false);
        var exit_button = new Gtk.Button();
        exit_button.set_can_focus(false);
        exit_button.clicked.connect(()=>{
            quit();
        });
        exit_button.add(new Gtk.Image.from_icon_name("window-close-symbolic",Gtk.IconSize.MENU));
        exit_button.set_relief(Gtk.ReliefStyle.NONE);
        header.pack_end(exit_button);

        // max - restore button
        var maxi_button = new Gtk.Button();
        maxi_button.set_can_focus(false);
        var maxi_icon = new Gtk.Image.from_icon_name("window-maximize-symbolic",Gtk.IconSize.MENU);
        maxi_button.clicked.connect(()=>{
            if(is_maximized){
                unmaximize();
                maxi_icon.set_from_icon_name("window-maximize-symbolic",Gtk.IconSize.MENU);
            }else{
                maximize();
                maxi_icon.set_from_icon_name("window-restore-symbolic",Gtk.IconSize.MENU);
            }
        });
        maxi_button.add(maxi_icon);
        maxi_button.set_relief(Gtk.ReliefStyle.NONE);
        header.pack_end(maxi_button);
        // mini button
        header.set_show_close_button(false);
        var mini_button = new Gtk.Button();
        mini_button.set_can_focus(false);
        mini_button.clicked.connect(()=>{
            iconify();
        });
        mini_button.add(new Gtk.Image.from_icon_name("window-minimize-symbolic",Gtk.IconSize.MENU));
        mini_button.set_relief(Gtk.ReliefStyle.NONE);
        header.pack_end(mini_button);
        
        set_titlebar(header);
        text_view = new Gtk.SourceView ();
        set_font(settings.get_string("font"));
        if(settings.get_boolean("word-wrap")){
            notepad.text_view.set_wrap_mode(Gtk.WrapMode.WORD);
        }else{
            notepad.text_view.set_wrap_mode(Gtk.WrapMode.NONE);
        }
        text_view.buffer.text = "";
        set_linenum();

        menu = new Gtk.MenuBar ();

        var scrolled_window = new Gtk.ScrolledWindow (null, null);
        scrolled_window.set_policy (Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.ALWAYS);
        scrolled_window.add (text_view);
        scrolled_window.hexpand = true;
        scrolled_window.vexpand = true;

        var grid = new Gtk.Grid ();
        grid.attach (menu, 0, 0, 1, 1);
        grid.attach (scrolled_window, 0, 1, 1, 1);
        add (grid as Gtk.Widget);
        delete_event.connect (quit);
    }

    public void set_font(string name){
        font = Pango.FontDescription.from_string(name);
        text_view.override_font(font);
    }

    public string get_name(){
        if(file != null){
            return file.get_path();
        }else{
            return "Untitled";
        }
    }


    public void set_highlight(){
        if(!settings.get_boolean("highlight")){
            ((Gtk.SourceBuffer)text_view.buffer).set_language(null);
            return;
        }
        var manager = new Gtk.SourceLanguageManager();
        string[] d = get_name().split(".");
        string suffix = d[d.length-1];
        stdout.printf(suffix);
        foreach(string lang in manager.language_ids){
            if(suffix == lang){
                ((Gtk.SourceBuffer)text_view.buffer).set_language(manager.get_language(lang));
            }
        }
    }

    public void set_linenum(){
        notepad.text_view.set_show_line_numbers(settings.get_boolean("line-number"));
    }

    public string get_selected_text(){
        Gtk.TextIter A, B;
        if (text_view.buffer.get_selection_bounds(out A, out B)){
           return text_view.buffer.get_text(A, B, true);
        }
        return "";
    }


    public bool is_empty(){
        return notepad.text_view.buffer.text.data.length != 0;
    }

    public int length (string text) {
        var text_buffer = new Gtk.TextBuffer (null);
        text_buffer.set_text (text);
        Gtk.TextIter count_iter;
        text_buffer.get_start_iter (out count_iter);

        var ret = 0;
        while (!count_iter.is_end ()) {
            ret++;
            count_iter.forward_cursor_position ();
        }

        return ret;
    }

    public bool quit(){
        var response = save_dialog_event();
        if(response){
            Gtk.main_quit();
        }
        return !response;
    }
}


