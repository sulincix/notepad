public NotePad notepad;
public FindWindow find_window;
public ReplaceWindow replace_window;
public GLib.Settings settings;
public Gtk.AccelGroup keyGroup; 
public static int main (string[] args) {
    GLib.Environment.set_variable("GTK_THEME","Adwaita",true);
    keyGroup = new Gtk.AccelGroup ();
    settings = new GLib.Settings("notepad.exe");
    Gtk.init (ref args);
    
    notepad = new NotePad ();
    notepad.init();
    notepad.add_accel_group(keyGroup);

    notepad.menu.add(file_menu());
    notepad.menu.add(edit_menu());
    notepad.menu.add(format_menu());
    notepad.menu.add(view_menu());
    notepad.menu.add(help_menu());
    notepad.set_icon_name("notepad.exe");

    style_init();
    notepad.show_all ();
    
    find_window = new FindWindow();
    find_window.init();
    replace_window = new ReplaceWindow();
    replace_window.init();
    if(args.length > 1){
        open_path(args[1]);
    }
    Gtk.main ();
    return 0;
}
