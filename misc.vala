public string url_encode(string text){
    string data = text;
    data = data.replace(" ","%20");
    data = data.replace("#","%23");
    data = data.replace("$","%24");
    data = data.replace("&","%26");
    data = data.replace("+","%2B");
    data = data.replace(",","%2C");
    data = data.replace("/","%2F");
    data = data.replace(":","%3A");
    data = data.replace(";","%3B");
    data = data.replace("=","%3D");
    data = data.replace("?","%3F");
    data = data.replace("@","%40");
    return data;
}
