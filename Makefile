SHELL=/bin/bash -e
DESTDIR=/
build: clean
	mkdir -p _build
	echo 'public string theme_light_data;' > _build/theme.vala
	echo 'public string theme_dark_data;' >> _build/theme.vala
	echo 'public void theme_init(){' >> _build/theme.vala
	echo '    theme_light_data = "' >> _build/theme.vala
	cat theme/light/*.css theme/*.css | sed "s|\"|\\\&|g" >> _build/theme.vala
	echo '";' >> _build/theme.vala
	echo '    theme_dark_data = "' >> _build/theme.vala
	cat theme/dark/*.css theme/*.css | sed "s|\"|\\\&|g" >> _build/theme.vala
	echo '";' >> _build/theme.vala
	echo '}' >> _build/theme.vala
	cd _build ; valac  -C `find ../ -type f -iname '*.vala'` theme.vala --pkg gtk+-3.0 --pkg gtksourceview-4

	for src in `ls _build/*.c` ; do \
	    echo $$src ;\
	    cd _build ; $(CC) -O2 -s $(CFLAGS) ../$$src -c `pkg-config --cflags gtk+-3.0 gtksourceview-4` ; cd .. ;\
	done
	$(CC) -O2 -s $(CFLAGS) _build/*.o -o _build/notepad `pkg-config --libs gtk+-3.0 gtksourceview-4`
	objcopy -R .comment -R .note -R .debug_info -R .debug_aranges -R .debug_pubnames  -R .debug_pubtypes \
	    -R .debug_abbrev -R .debug_line -R .debug_str -R .debug_ranges -R .debug_loc _build/notepad
	mv _build build

install:
	mkdir -p $(DESTDIR)/usr/share/glib-2.0/schemas/
	mkdir -p $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/share/applications/
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/
	install build/notepad $(DESTDIR)/usr/bin/notepad.exe
	install gsettings.xml $(DESTDIR)/usr/share/glib-2.0/schemas/notepad.exe.gschema.xml
	install application.desktop $(DESTDIR)/usr/share/applications/notepad.exe.desktop
	install icon.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/notepad.exe.svg
	if [[ $(DESTDIR) == / ]] ; then \
	    glib-compile-schemas /usr/share/glib-2.0/schemas/ ;\
	    gtk-update-icon-cache /usr/share/icons/hicolor/ ;\
	fi
clean:
	rm -rf build _build

